# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
# Shinjo Park <kde@peremen.name>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-09 01:03+0000\n"
"PO-Revision-Date: 2021-11-14 18:03+0100\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 21.08.1\n"

#: contents/config/config.qml:6
#, kde-format
msgid "General"
msgstr "일반"

#: contents/ui/configGeneral.qml:30
#, kde-format
msgid "Display style:"
msgstr "표시 스타일:"

#: contents/ui/configGeneral.qml:70
#, kde-format
msgctxt "@info:placeholder Make this translation as short as possible"
msgid "No flag available"
msgstr ""

#: contents/ui/configGeneral.qml:111
#, kde-format
msgid "Layouts:"
msgstr "레이아웃:"

#: contents/ui/configGeneral.qml:112
#, kde-format
msgid "Configure…"
msgstr "설정…"

#, fuzzy
#~| msgid "Show flag"
#~ msgid "Show label"
#~ msgstr "국기 표시"

#~ msgid "Show flag"
#~ msgstr "국기 표시"
