# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Alexander Potashev <aspotashev@gmail.com>, 2014, 2017, 2018.
# Alexander Yavorsky <kekcuha@gmail.com>, 2018, 2019, 2020, 2021, 2022.
# Дронова Ю <juliette.tux@gmail.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-17 00:51+0000\n"
"PO-Revision-Date: 2022-12-23 11:02+0300\n"
"Last-Translator: Alexander Yavorsky <kekcuha@gmail.com>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 21.08.3\n"

#: package/contents/ui/main.qml:27
#, kde-format
msgid ""
"The system must be restarted before changes to the middle-click paste "
"setting can take effect."
msgstr ""
"Для применения параметра вставки щелчком средней клавишей мыши требуется "
"перезагрузка системы."

#: package/contents/ui/main.qml:33
#, kde-format
msgid "Restart"
msgstr "Перезагрузить"

#: package/contents/ui/main.qml:50
#, kde-format
msgid "Visual behavior:"
msgstr "Визуальное поведение:"

#. i18n: ectx: label, entry (delay), group (PlasmaToolTips)
#: package/contents/ui/main.qml:51 workspaceoptions_plasmasettings.kcfg:9
#, kde-format
msgid "Display informational tooltips on mouse hover"
msgstr "Всплывающие подсказки при наведении курсора мыши"

#. i18n: ectx: label, entry (osdEnabled), group (OSD)
#: package/contents/ui/main.qml:62 workspaceoptions_plasmasettings.kcfg:15
#, kde-format
msgid "Display visual feedback for status changes"
msgstr "Показывать уведомления при регулировке устройств"

#: package/contents/ui/main.qml:80
#, kde-format
msgid "Animation speed:"
msgstr "Скорость анимации:"

#: package/contents/ui/main.qml:103
#, kde-format
msgctxt "Animation speed"
msgid "Slow"
msgstr "Медленнее"

#: package/contents/ui/main.qml:109
#, kde-format
msgctxt "Animation speed"
msgid "Instant"
msgstr "Быстрее"

#: package/contents/ui/main.qml:124
#, kde-format
msgctxt ""
"part of a sentence: 'Clicking files or folders [opens them/selects them]'"
msgid "Clicking files or folders:"
msgstr "Щелчок по файлу или папке:"

#: package/contents/ui/main.qml:125
#, kde-format
msgctxt "part of a sentence: 'Clicking files or folders opens them'"
msgid "Opens them"
msgstr "Открывает объект"

#: package/contents/ui/main.qml:137
#, kde-format
msgid "Select by clicking on item's selection marker"
msgstr "Выбирать щелчком по маркёру выделения"

#: package/contents/ui/main.qml:147
#, kde-format
msgctxt "part of a sentence: 'Clicking files or folders selects them'"
msgid "Selects them"
msgstr "Выделяет объект"

#: package/contents/ui/main.qml:160
#, kde-format
msgid "Open by double-clicking instead"
msgstr "Открывать двойным щелчком"

#: package/contents/ui/main.qml:175
#, kde-format
msgid "Clicking in scrollbar track:"
msgstr "Щелчок по полосе прокрутки:"

#: package/contents/ui/main.qml:176
#, kde-format
msgctxt ""
"@radio part of a complete sentence: 'Clicking in scrollbar track scrolls one "
"page up or down'"
msgid "Scrolls one page up or down"
msgstr "Прокручивает на одну страницу страницу вверх или вниз"

#: package/contents/ui/main.qml:188
#, kde-format
msgid "Middle-click to scroll to clicked location"
msgstr "Прокручивание в заданное положение щелчком средней кнопки"

#: package/contents/ui/main.qml:198
#, kde-format
msgctxt ""
"@radio part of a complete sentence: 'Clicking in scrollbar track scrolls to "
"the clicked location'"
msgid "Scrolls to the clicked location"
msgstr "Прокручивает в указанное положение"

#: package/contents/ui/main.qml:217
#, kde-format
msgid "Middle Click:"
msgstr "Щелчок средней кнопкой:"

#: package/contents/ui/main.qml:219
#, kde-format
msgid "Paste selected text"
msgstr "Вставляет выделенный текст"

#: package/contents/ui/main.qml:236
#, kde-format
msgid "Touch Mode:"
msgstr "Сенсорный режим работы:"

#: package/contents/ui/main.qml:238
#, kde-format
msgctxt "As in: 'Touch Mode is automatically enabled as needed'"
msgid "Automatically enable as needed"
msgstr "Автоматически"

#: package/contents/ui/main.qml:238 package/contents/ui/main.qml:267
#, kde-format
msgctxt "As in: 'Touch Mode is never enabled'"
msgid "Never enabled"
msgstr "Никогда не использовать"

#: package/contents/ui/main.qml:250
#, kde-format
msgid ""
"Touch Mode will be automatically activated whenever the system detects a "
"touchscreen but no mouse or touchpad. For example: when a transformable "
"laptop's keyboard is flipped around or detached."
msgstr ""
"Сенсорный режим работы будет автоматически включён при наличии сенсорного "
"экрана если мышь или сенсорная панель отключены, например в случае "
"переворачивания экрана или отключения клавиатуры от ноутбука-трансформера."

#: package/contents/ui/main.qml:255
#, kde-format
msgctxt "As in: 'Touch Mode is always enabled'"
msgid "Always enabled"
msgstr "Использовать всегда"

#: package/contents/ui/main.qml:280
#, kde-format
msgid ""
"In Touch Mode, many elements of the user interface will become larger to "
"more easily accommodate touch interaction."
msgstr ""
"В сенсорном режиме работы большое количество элементов пользовательского "
"интерфейса становятся крупнее, чтобы облегчить работу с ними."

#. i18n: ectx: label, entry (singleClick), group (KDE)
#: workspaceoptions_kdeglobalssettings.kcfg:9
#, kde-format
msgid "Single click to open files"
msgstr "Одинарный щелчок для открытия файлов"

#. i18n: ectx: label, entry (animationDurationFactor), group (KDE)
#: workspaceoptions_kdeglobalssettings.kcfg:13
#, kde-format
msgid "Animation speed"
msgstr "Скорость анимации"

#. i18n: ectx: label, entry (scrollbarLeftClickNavigatesByPage), group (KDE)
#: workspaceoptions_kdeglobalssettings.kcfg:17
#, kde-format
msgid "Left-click in scrollbar track moves scrollbar by one page"
msgstr "Прокрутка на одну страницу по щелчку по полосе прокрутки"

#. i18n: ectx: label, entry (tabletMode), group (Input)
#: workspaceoptions_kwinsettings.kcfg:9
#, kde-format
msgid "Automatically switch to touch-optimized mode"
msgstr "Автоматическое переключение в сенсорный режим работы"

#. i18n: ectx: label, entry (primarySelection), group (Wayland)
#: workspaceoptions_kwinsettings.kcfg:15
#, kde-format
msgid "Enable middle click selection pasting"
msgstr "Щелчок средней кнопкой вставляет выделенный текст"

#. i18n: ectx: tooltip, entry (osdEnabled), group (OSD)
#: workspaceoptions_plasmasettings.kcfg:16
#, kde-format
msgid ""
"Show an on-screen display to indicate status changes such as brightness or "
"volume"
msgstr ""
"Показывать экранные уведомления об изменении, например, яркости экрана или "
"громкости."

#. i18n: ectx: label, entry (osdKbdLayoutChangedEnabled), group (OSD)
#: workspaceoptions_plasmasettings.kcfg:20
#, kde-format
msgid "OSD on layout change"
msgstr "Экранные уведомления при смене раскладки"

#. i18n: ectx: tooltip, entry (osdKbdLayoutChangedEnabled), group (OSD)
#: workspaceoptions_plasmasettings.kcfg:21
#, kde-format
msgid "Show a popup on layout changes"
msgstr "Показывать экранные уведомления при переключении раскладки клавиатуры"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Александр Поташев,Дронова Юлия"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "aspotashev@gmail.com,juliette.tux@gmail.com"

#~ msgid "General Behavior"
#~ msgstr "Основные параметры"

#~ msgid "System Settings module for configuring general workspace behavior."
#~ msgstr ""
#~ "Модуль приложения «Параметры системы» для настройки основных параметров "
#~ "поведения рабочей среды Plasma."

#~ msgid "Furkan Tokac"
#~ msgstr "Furkan Tokac"

#~ msgid "Click behavior:"
#~ msgstr "Поведение при нажатиях:"

#~ msgid "Double-click to open files and folders"
#~ msgstr "Двойной щелчок для открытия файлов и каталогов"

#~ msgid "Select by single-clicking"
#~ msgstr "Выбор одинарным щелчком"

#~ msgid "Double-click to open files and folders (single click to select)"
#~ msgstr ""
#~ "Двойной щелчок для открытия файлов и каталогов (одинарный щелчок для "
#~ "выделения)"

#~ msgid "Plasma Workspace global options"
#~ msgstr "Главные параметры среды Plasma"

#~ msgid "(c) 2009 Marco Martin"
#~ msgstr "© Marco Martin, 2009"

#~ msgid "Marco Martin"
#~ msgstr "Marco Martin"

#~ msgid "Maintainer"
#~ msgstr "Сопровождающий"

#~ msgid "Show Informational Tips"
#~ msgstr "Показывать всплывающие подсказки"
