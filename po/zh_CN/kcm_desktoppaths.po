msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-06 01:02+0000\n"
"PO-Revision-Date: 2023-02-15 11:05\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/plasma-desktop/kcm_desktoppaths.pot\n"
"X-Crowdin-File-ID: 4349\n"

#: desktoppathssettings.cpp:211
#, kde-format
msgid "Desktop"
msgstr "桌面"

#: desktoppathssettings.cpp:226
#, kde-format
msgid "Documents"
msgstr "文档"

#: desktoppathssettings.cpp:241
#, kde-format
msgid "Downloads"
msgstr "下载"

#: desktoppathssettings.cpp:256
#, kde-format
msgid "Music"
msgstr "音乐"

#: desktoppathssettings.cpp:271
#, kde-format
msgid "Pictures"
msgstr "图片"

#: desktoppathssettings.cpp:286
#, kde-format
msgid "Videos"
msgstr "视频"

#: desktoppathssettings.cpp:301
#, kde-format
msgid "Public"
msgstr "公共"

#: desktoppathssettings.cpp:316
#, kde-format
msgid "Templates"
msgstr "模板"

#: globalpaths.cpp:27
#, kde-format
msgid ""
"<h1>Paths</h1>\n"
"This module allows you to choose where in the filesystem the files on your "
"desktop should be stored.\n"
"Use the \"Whats This?\" (Shift+F1) to get help on specific options."
msgstr ""
"<h1>路径</h1>\n"
"此模块允许您选择要在文件系统的什么地方存储桌面上的文件。\n"
"请使用“这是什么？”(Shift+F1)在特定选项取得帮助。"

#: package/contents/ui/main.qml:22
#, kde-format
msgid "Desktop path:"
msgstr "桌面路径："

#: package/contents/ui/main.qml:25
#, kde-format
msgid ""
"This folder contains all the files which you see on your desktop. You can "
"change the location of this folder if you want to, and the contents will "
"move automatically to the new location as well."
msgstr ""
"该文件夹中包含了桌面上显示的所有文件。您可以更改该文件夹的位置，原文件夹内容"
"将会自动移动到新的位置。"

#: package/contents/ui/main.qml:31
#, kde-format
msgid "Documents path:"
msgstr "文档路径："

#: package/contents/ui/main.qml:34
#, kde-format
msgid ""
"This folder will be used by default to load or save documents from or to."
msgstr "该文件夹默认将被用来加载或者保存文档。"

#: package/contents/ui/main.qml:40
#, kde-format
msgid "Downloads path:"
msgstr "下载路径："

#: package/contents/ui/main.qml:43
#, kde-format
msgid "This folder will be used by default to save your downloaded items."
msgstr "该文件夹默认将被用来保存下载到本机的内容。"

#: package/contents/ui/main.qml:49
#, kde-format
msgid "Videos path:"
msgstr "视频路径："

#: package/contents/ui/main.qml:52 package/contents/ui/main.qml:79
#, kde-format
msgid "This folder will be used by default to load or save movies from or to."
msgstr "该文件夹默认将被用来加载或者保存影片。"

#: package/contents/ui/main.qml:58
#, kde-format
msgid "Pictures path:"
msgstr "图片路径："

#: package/contents/ui/main.qml:61
#, kde-format
msgid ""
"This folder will be used by default to load or save pictures from or to."
msgstr "该文件夹默认将被用来加载或者保存图片。"

#: package/contents/ui/main.qml:67
#, kde-format
msgid "Music path:"
msgstr "音乐路径："

#: package/contents/ui/main.qml:70
#, kde-format
msgid "This folder will be used by default to load or save music from or to."
msgstr "该文件夹默认将被用来加载或者保存音乐。"

#: package/contents/ui/main.qml:76
#, kde-format
msgid "Public path:"
msgstr "公共路径："

#: package/contents/ui/main.qml:85
#, kde-format
msgid "Templates path:"
msgstr "模板路径："

#: package/contents/ui/main.qml:88
#, kde-format
msgid ""
"This folder will be used by default to load or save templates from or to."
msgstr "该文件夹默认将被用来加载或者保存模板。"

#: package/contents/ui/UrlRequester.qml:66
#, kde-format
msgctxt "@action:button"
msgid "Choose new location"
msgstr "选择新位置"
