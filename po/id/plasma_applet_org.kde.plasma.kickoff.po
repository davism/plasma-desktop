# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
# Lorenz Adam Damara <lorenzrenz@gmail.com>, 2018.
# Wantoyo <wantoyek@gmail.com>, 2018, 2019, 2021, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-24 01:00+0000\n"
"PO-Revision-Date: 2022-03-24 18:50+0700\n"
"Last-Translator: Wantoyèk <wantoyek@gmail.com>\n"
"Language-Team: Indonesian <kde-i18n-doc@kde.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr "Umum"

#: package/contents/ui/code/tools.js:49
#, kde-format
msgid "Remove from Favorites"
msgstr "Hapus dari Favorit"

#: package/contents/ui/code/tools.js:53
#, kde-format
msgid "Add to Favorites"
msgstr "Tambahkan ke Favorit"

#: package/contents/ui/code/tools.js:77
#, kde-format
msgid "On All Activities"
msgstr "Di Semua Activities"

#: package/contents/ui/code/tools.js:127
#, kde-format
msgid "On the Current Activity"
msgstr "Di Aktivitas Saat Ini"

#: package/contents/ui/code/tools.js:141
#, kde-format
msgid "Show in Favorites"
msgstr "Tampilkan di Favorit"

#: package/contents/ui/ConfigGeneral.qml:37
#, kde-format
msgid "Icon:"
msgstr "Ikon:"

#: package/contents/ui/ConfigGeneral.qml:43
#, kde-format
msgctxt "@action:button"
msgid "Change Application Launcher's icon"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:44
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"Current icon is %1. Click to open menu to change the current icon or reset "
"to the default icon."
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:48
#, kde-format
msgctxt "@info:tooltip"
msgid "Icon name is \"%1\""
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:81
#, kde-format
msgctxt "@item:inmenu Open icon chooser dialog"
msgid "Choose…"
msgstr "Pilih..."

#: package/contents/ui/ConfigGeneral.qml:83
#, kde-format
msgctxt "@info:whatsthis"
msgid "Choose an icon for Application Launcher"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:87
#, kde-format
msgctxt "@item:inmenu Reset icon to default"
msgid "Reset to default icon"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:93
#, kde-format
msgctxt "@action:inmenu"
msgid "Remove icon"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:104
#, kde-format
msgctxt "@label:textbox"
msgid "Text label:"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:106
#, kde-format
msgctxt "@info:placeholder"
msgid "Type here to add a text label"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:121
#, kde-format
msgctxt "@action:button"
msgid "Reset menu label"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:135
#, kde-format
msgctxt "@info"
msgid "A text label cannot be set when the Panel is vertical."
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:146
#, fuzzy, kde-format
#| msgid "General:"
msgctxt "General options"
msgid "General:"
msgstr "Umum:"

#: package/contents/ui/ConfigGeneral.qml:147
#, kde-format
msgid "Always sort applications alphabetically"
msgstr "Selalu urutkan aplikasi berdasarkan alfabet"

#: package/contents/ui/ConfigGeneral.qml:152
#, kde-format
msgid "Use compact list item style"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:158
#, kde-format
msgctxt "@info:usagetip under a checkbox when Touch Mode is on"
msgid "Automatically disabled when in Touch Mode"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:167
#, kde-format
msgctxt "@action:button"
msgid "Configure Enabled Search Plugins…"
msgstr "Konfigurasikan Plugin Pencarian Yang Difungsikan..."

#: package/contents/ui/ConfigGeneral.qml:177
#, kde-format
msgid "Show favorites:"
msgstr "Tampilkan favorit:"

#: package/contents/ui/ConfigGeneral.qml:178
#, kde-format
msgctxt "Part of a sentence: 'Show favorites in a grid'"
msgid "In a grid"
msgstr "Dalam sebuah kisi"

#: package/contents/ui/ConfigGeneral.qml:186
#, kde-format
msgctxt "Part of a sentence: 'Show favorites in a list'"
msgid "In a list"
msgstr "Dalam sebuah daftar"

#: package/contents/ui/ConfigGeneral.qml:194
#, kde-format
msgid "Show other applications:"
msgstr "Tampilkan aplikasi lainnya:"

#: package/contents/ui/ConfigGeneral.qml:195
#, kde-format
msgctxt "Part of a sentence: 'Show other applications in a grid'"
msgid "In a grid"
msgstr "Dalam sebuah kisi"

#: package/contents/ui/ConfigGeneral.qml:203
#, kde-format
msgctxt "Part of a sentence: 'Show other applications in a list'"
msgid "In a list"
msgstr "Dalam sebuah daftar"

#: package/contents/ui/ConfigGeneral.qml:215
#, kde-format
msgid "Show buttons for:"
msgstr "Tampilkan tombol untuk:"

#: package/contents/ui/ConfigGeneral.qml:216
#: package/contents/ui/LeaveButtons.qml:115
#, kde-format
msgid "Power"
msgstr "Daya"

#: package/contents/ui/ConfigGeneral.qml:225
#, kde-format
msgid "Session"
msgstr "Sesi"

#: package/contents/ui/ConfigGeneral.qml:234
#, kde-format
msgid "Power and session"
msgstr "Daya dan sesi"

#: package/contents/ui/ConfigGeneral.qml:243
#, kde-format
msgid "Show action button captions"
msgstr "Tampilkan bab tombol aksi"

#: package/contents/ui/Footer.qml:97
#, kde-format
msgid "Applications"
msgstr "Aplikasi"

#: package/contents/ui/Footer.qml:110
#, kde-format
msgid "Places"
msgstr "Tempat"

#: package/contents/ui/FullRepresentation.qml:119
#, kde-format
msgctxt "@info:status"
msgid "No matches"
msgstr ""

#: package/contents/ui/Header.qml:64
#, kde-format
msgid "Open user settings"
msgstr "Buka pengaturan pengguna"

#: package/contents/ui/Header.qml:240
#, kde-format
msgid "Keep Open"
msgstr "Tetap Buka"

#: package/contents/ui/Kickoff.qml:302
#, kde-format
msgid "Edit Applications…"
msgstr "Edit Aplikasi..."

#: package/contents/ui/KickoffGridView.qml:88
#, kde-format
msgid "Grid with %1 rows, %2 columns"
msgstr "Kisikan dengan %1 baris, %2 kolom"

#: package/contents/ui/LeaveButtons.qml:115
#, kde-format
msgid "Leave"
msgstr "Tinggalkan"

#: package/contents/ui/LeaveButtons.qml:115
#, kde-format
msgid "More"
msgstr "Selebihnya"

#: package/contents/ui/PlacesPage.qml:50
#, kde-format
msgctxt "category in Places sidebar"
msgid "Computer"
msgstr "Komputer"

#: package/contents/ui/PlacesPage.qml:51
#, kde-format
msgctxt "category in Places sidebar"
msgid "History"
msgstr "Histori"

#: package/contents/ui/PlacesPage.qml:52
#, kde-format
msgctxt "category in Places sidebar"
msgid "Frequently Used"
msgstr "Sering Digunakan"

#~ msgctxt "@item:inmenu Reset icon to default"
#~ msgid "Clear Icon"
#~ msgstr "Bersihkan Ikon"

#, fuzzy
#~| msgid "Search..."
#~ msgid "Search…"
#~ msgstr "Cari..."

#~ msgid "Primary actions:"
#~ msgstr "Aksi primer:"

#, fuzzy
#~| msgid "Leave"
#~ msgid "Leave…"
#~ msgstr "Tinggalkan"

#, fuzzy
#~| msgid "Power"
#~ msgid "Power…"
#~ msgstr "Daya"

#, fuzzy
#~| msgid "More"
#~ msgid "More…"
#~ msgstr "Selebihnya"

#, fuzzy
#~| msgid "Edit Applications..."
#~ msgid "Updating applications…"
#~ msgstr "Ubah Aplikasi..."

#~ msgid "Allow labels to have two lines"
#~ msgstr "Izinkan label untuk memiliki dua baris"

#~ msgid "%2@%3 (%1)"
#~ msgstr "%2@%3 (%1)"

#~ msgid "%1@%2"
#~ msgstr "%1@%2"

#~ msgid "List with 1 item"
#~ msgid_plural "List with %1 items"
#~ msgstr[0] "Daftarkan dengan 1 item"
#~ msgstr[1] "Daftarkan dengan %1 item"

#~ msgid "%1 submenu"
#~ msgstr "%1 submenu"

#~ msgid "Leave..."
#~ msgstr "Tinggalkan..."

#~ msgid "Power..."
#~ msgstr "Daya..."

#~ msgid "More..."
#~ msgstr "Selebihnya..."

#~ msgid "Applications updated."
#~ msgstr "Aplikasi diperbarui."

#~ msgid "Switch tabs on hover"
#~ msgstr "Beralih tab pada saat kursor melayang diatasnya"

#~ msgid "All Applications"
#~ msgstr "Semua Aplikasi"

#~ msgid "Favorites"
#~ msgstr "Favorit"

#~ msgid "Often Used"
#~ msgstr "Sering Digunakan"

#~ msgid "Active Tabs"
#~ msgstr "Tab-tab Aktif"

#~ msgid "Inactive Tabs"
#~ msgstr "Tab-tab Takaktif"

#~ msgid ""
#~ "Drag tabs between the boxes to show/hide them, or reorder the visible "
#~ "tabs by dragging."
#~ msgstr ""
#~ "Seret tab diantara kotak untuk menampilkan/menyembunyikan nya, atau "
#~ "urutkan kembali tab yang terlihat dengan menyeretnya."

#~ msgid "Expand search to bookmarks, files and emails"
#~ msgstr "Perluas pencarian untuk markah, berkas dan surel"

#~ msgid "Appearance"
#~ msgstr "Tampilan"

#~ msgid "Menu Buttons"
#~ msgstr "Tombol Menu"

#~ msgid "Visible Tabs"
#~ msgstr "Tab yang Terlihat"
