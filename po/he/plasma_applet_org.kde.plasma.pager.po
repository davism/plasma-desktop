# translation of plasma_applet_pager.po to hebrew
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Diego Iastrubni <elcuco@kde.org>, 2008, 2013.
# tahmar1900 <tahmar1900@gmail.com>, 2008.
# Tahmar1900 <tahmar1900@gmail.com>, 2011.
# elkana bardugo <ttv200@gmail.com>, 2016.
# Elkana Bardugo <ttv200@gmail.com>, 2017. #zanata
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org.kde.plasma.pager\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-18 00:46+0000\n"
"PO-Revision-Date: 2017-05-22 05:25-0400\n"
"Last-Translator: Elkana Bardugo <ttv200@gmail.com>\n"
"Language-Team: Hebrew <kde-i18n-doc@kde.org>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Zanata 3.9.6\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr "כללי"

#: package/contents/ui/configGeneral.qml:40
#, fuzzy, kde-format
#| msgid "General"
msgid "General:"
msgstr "כללי"

#: package/contents/ui/configGeneral.qml:42
#, kde-format
msgid "Show application icons on window outlines"
msgstr ""

#: package/contents/ui/configGeneral.qml:47
#, fuzzy, kde-format
#| msgid "Only the current screen"
msgid "Show only current screen"
msgstr "רק במסך הנוכחי"

#: package/contents/ui/configGeneral.qml:52
#, kde-format
msgid "Navigation wraps around"
msgstr ""

#: package/contents/ui/configGeneral.qml:64
#, kde-format
msgid "Layout:"
msgstr "פריסה:"

#: package/contents/ui/configGeneral.qml:66
#, kde-format
msgctxt "The pager layout"
msgid "Default"
msgstr "ברירת מחדל"

#: package/contents/ui/configGeneral.qml:66
#, kde-format
msgid "Horizontal"
msgstr "אופקי"

#: package/contents/ui/configGeneral.qml:66
#, kde-format
msgid "Vertical"
msgstr "אנכי"

#: package/contents/ui/configGeneral.qml:80
#, fuzzy, kde-format
#| msgid "Display:"
msgid "Text display:"
msgstr "תצוגות:"

#: package/contents/ui/configGeneral.qml:83
#, kde-format
msgid "No text"
msgstr "ללא טקסט"

#: package/contents/ui/configGeneral.qml:91
#, kde-format
msgid "Activity number"
msgstr "מספר פעילות"

#: package/contents/ui/configGeneral.qml:91
#, kde-format
msgid "Desktop number"
msgstr "שולחן עבודה מספר"

#: package/contents/ui/configGeneral.qml:99
#, kde-format
msgid "Activity name"
msgstr "שם פעילות"

#: package/contents/ui/configGeneral.qml:99
#, kde-format
msgid "Desktop name"
msgstr "שם שולחן העבודה"

#: package/contents/ui/configGeneral.qml:113
#, fuzzy, kde-format
#| msgid "Selecting current desktop:"
msgid "Selecting current Activity:"
msgstr "בוחר את שולחן העבודה הנוכחי:"

#: package/contents/ui/configGeneral.qml:113
#, fuzzy, kde-format
#| msgid "Selecting current desktop:"
msgid "Selecting current virtual desktop:"
msgstr "בוחר את שולחן העבודה הנוכחי:"

#: package/contents/ui/configGeneral.qml:116
#, kde-format
msgid "Does nothing"
msgstr "לא עושה כלום"

#: package/contents/ui/configGeneral.qml:124
#, fuzzy, kde-format
#| msgid "Shows desktop"
msgid "Shows the desktop"
msgstr "מראה שולחן־עבודה"

#: package/contents/ui/main.qml:104
#, fuzzy, kde-format
#| msgid "...and %1 other window"
#| msgid_plural "...and %1 other windows"
msgid "…and %1 other window"
msgid_plural "…and %1 other windows"
msgstr[0] "...וחלון אחד אחר"
msgstr[1] "...ו־%1 חלונות אחרים"

#: package/contents/ui/main.qml:362
#, kde-format
msgid "%1 Window:"
msgid_plural "%1 Windows:"
msgstr[0] "חלון אחד:"
msgstr[1] "%1 חלונות"

#: package/contents/ui/main.qml:374
#, kde-format
msgid "%1 Minimized Window:"
msgid_plural "%1 Minimized Windows:"
msgstr[0] "%1 חלון ממוזער"
msgstr[1] "%1 חלונות ממוזערים"

#: package/contents/ui/main.qml:449
#, fuzzy, kde-format
#| msgid "Desktop name"
msgid "Desktop %1"
msgstr "שם שולחן העבודה"

#: package/contents/ui/main.qml:450
#, kde-format
msgctxt "@info:tooltip %1 is the name of a virtual desktop or an activity"
msgid "Switch to %1"
msgstr ""

#: package/contents/ui/main.qml:595
#, fuzzy, kde-format
#| msgid "Show Activity Manager..."
msgid "Show Activity Manager…"
msgstr "הראה מנהל פעילויות..."

#: package/contents/ui/main.qml:598
#, kde-format
msgid "Add Virtual Desktop"
msgstr "הוסף שולחן עבודה וירטואלי"

#: package/contents/ui/main.qml:599
#, kde-format
msgid "Remove Virtual Desktop"
msgstr "&הסר את שולחן עבודה וירטואלי"

#: package/contents/ui/main.qml:602
#, fuzzy, kde-format
#| msgid "Configure Desktops..."
msgid "Configure Virtual Desktops…"
msgstr "הגדרת שולחנות עבודה..."

#~ msgid "Icons"
#~ msgstr "סמלים"
