# translation of kcmkded.po to Frysk
#
# Rinse de Vries <rinsedevries@kde.nl>, 2005, 2007.
msgid ""
msgstr ""
"Project-Id-Version: kcmkded\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-05-04 00:47+0000\n"
"PO-Revision-Date: 2009-06-12 09:24+0100\n"
"Last-Translator: Berend Ytsma <berendy@gmail.com>\n"
"Language-Team: Frysk <kde-i18n-fry@kde.org>\n"
"Language: fy\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Berend Ytsma"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "berendy@bigfoot.com"

#: kcmkded.cpp:48
#, fuzzy, kde-format
#| msgid "Startup Services"
msgid "Background Services"
msgstr "Start tsjinsten"

#: kcmkded.cpp:52
#, fuzzy, kde-format
#| msgid "(c) 2002 Daniel Molkentin"
msgid "(c) 2002 Daniel Molkentin, (c) 2020 Kai Uwe Broulik"
msgstr "(c) 2002 Daniel Molkentin"

#: kcmkded.cpp:53
#, kde-format
msgid "Daniel Molkentin"
msgstr "Daniel Molkentin"

#: kcmkded.cpp:54
#, kde-format
msgid "Kai Uwe Broulik"
msgstr ""

#: kcmkded.cpp:125
#, fuzzy, kde-format
#| msgid "Unable to stop server <em>%1</em>."
msgid "Failed to stop service: %1"
msgstr "It ophâlden fan tsjinner <em>%1</em> slagge net."

#: kcmkded.cpp:127
#, fuzzy, kde-format
#| msgid "Unable to start server <em>%1</em>."
msgid "Failed to start service: %1"
msgstr "It úteinsetten fan tsjinner <em>%1</em> slagge net."

#: kcmkded.cpp:134
#, fuzzy, kde-format
#| msgid "Unable to stop server <em>%1</em>."
msgid "Failed to stop service."
msgstr "It ophâlden fan tsjinner <em>%1</em> slagge net."

#: kcmkded.cpp:136
#, fuzzy, kde-format
#| msgid "Unable to start server <em>%1</em>."
msgid "Failed to start service."
msgstr "It úteinsetten fan tsjinner <em>%1</em> slagge net."

#: kcmkded.cpp:234
#, kde-format
msgid "Failed to notify KDE Service Manager (kded5) of saved changed: %1"
msgstr ""

#: package/contents/ui/main.qml:19
#, fuzzy, kde-format
#| msgid ""
#| "<h1>Service Manager</h1><p>This module allows you to have an overview of "
#| "all plugins of the KDE Daemon, also referred to as KDE Services. "
#| "Generally, there are two types of service:</p><ul><li>Services invoked at "
#| "startup</li><li>Services called on demand</li></ul><p>The latter are only "
#| "listed for convenience. The startup services can be started and stopped. "
#| "In Administrator mode, you can also define whether services should be "
#| "loaded at startup.</p><p><b> Use this with care: some services are vital "
#| "for KDE; do not deactivate services if you do not know what you are doing."
#| "</b></p>"
msgid ""
"<p>This module allows you to have an overview of all plugins of the KDE "
"Daemon, also referred to as KDE Services. Generally, there are two types of "
"service:</p> <ul><li>Services invoked at startup</li><li>Services called on "
"demand</li></ul> <p>The latter are only listed for convenience. The startup "
"services can be started and stopped. You can also define whether services "
"should be loaded at startup.</p> <p><b>Use this with care: some services are "
"vital for Plasma; do not deactivate services if you  do not know what you "
"are doing.</b></p>"
msgstr ""
"<h1>Tsjinstbehear</h1><p>Yn dizze module kinne jo in oersicht krije fan alle "
"plugins fan KDE Daemon, Dizzen wurde ek wol KDE Tsjinsten neamd. Yn prinsipe "
"binne der twa soarten fan tsjinsten:</p><ul><li>Tsjinsten dy ûnder it "
"begjinnen laden wurde</li><li>Tsjinsten dy op ôfrop laden wurde</li></"
"ul><p>De lêsten wurde hjir allinne foar it gemak efkes sjen litten. De "
"begjintsjinsten kinne begon en eine wurde. Yn de systeembeheardermodus kinne "
"jo ek bepale hokker tsjinsten ûnder it begjinnen moatte laden wurde. </"
"p><p><b> Wês foarsichtich, Guon tsjinsten binne fitaal foar KDE. Skeakelje "
"gjin tsjinsten út as jo net witte wat jo dogge.</b></p>"

#: package/contents/ui/main.qml:38
#, kde-format
msgid ""
"The background services manager (kded5) is currently not running. Make sure "
"it is installed correctly."
msgstr ""

#: package/contents/ui/main.qml:47
#, kde-format
msgid ""
"Some services disable themselves again when manually started if they are not "
"useful in the current environment."
msgstr ""

#: package/contents/ui/main.qml:56
#, kde-format
msgid ""
"Some services were automatically started/stopped when the background "
"services manager (kded5) was restarted to apply your changes."
msgstr ""

#: package/contents/ui/main.qml:98
#, fuzzy, kde-format
#| msgid "Service"
msgid "All Services"
msgstr "Tsjinst"

#: package/contents/ui/main.qml:99
#, fuzzy, kde-format
#| msgid "Running"
msgctxt "List running services"
msgid "Running"
msgstr "Aktyf"

#: package/contents/ui/main.qml:100
#, fuzzy, kde-format
#| msgid "Not running"
msgctxt "List not running services"
msgid "Not Running"
msgstr "Net aktyf"

#: package/contents/ui/main.qml:136
#, fuzzy, kde-format
#| msgid "Startup Services"
msgid "Startup Services"
msgstr "Start tsjinsten"

#: package/contents/ui/main.qml:137
#, kde-format
msgid "Load-on-Demand Services"
msgstr "Tsjinsten dy op fersyk laden wurde"

#: package/contents/ui/main.qml:154
#, kde-format
msgid "Toggle automatically loading this service on startup"
msgstr ""

#: package/contents/ui/main.qml:221
#, kde-format
msgid "Not running"
msgstr "Net aktyf"

#: package/contents/ui/main.qml:222
#, kde-format
msgid "Running"
msgstr "Aktyf"

#: package/contents/ui/main.qml:240
#, fuzzy, kde-format
#| msgid "Service"
msgid "Stop Service"
msgstr "Tsjinst"

#: package/contents/ui/main.qml:240
#, fuzzy, kde-format
#| msgid "Service"
msgid "Start Service"
msgstr "Tsjinst"

#~ msgid "kcmkded"
#~ msgstr "kcmkded"

#~ msgid "KDE Service Manager"
#~ msgstr "KDE-tsjinstbehear"

#~ msgid ""
#~ "This is a list of available KDE services which will be started on demand. "
#~ "They are only listed for convenience, as you cannot manipulate these "
#~ "services."
#~ msgstr ""
#~ "Dit is in list mei beskikbere KDE-tsjinsten dy't op fersyk start wurde. "
#~ "Se wurde hjir allinne sjen litten, jo kinne dizze tsjinsten net wizigje."

#~ msgid "Status"
#~ msgstr "Tastân"

#~ msgid "Description"
#~ msgstr "Beskriuwing"

#, fuzzy
#~| msgid ""
#~| "This shows all KDE services that can be loaded on KDE startup. Checked "
#~| "services will be invoked on next startup. Be careful with deactivation "
#~| "of unknown services."
#~ msgid ""
#~ "This shows all KDE services that can be loaded on Plasma startup. Checked "
#~ "services will be invoked on next startup. Be careful with deactivation of "
#~ "unknown services."
#~ msgstr ""
#~ "Hjir wurde alle KDE-tsjinsten sjen litten dy ûnder it begjinnen fan KDE "
#~ "laden wurde. Markearre tsjinsten wurde by in folgjend begjinnen  fan KDE "
#~ "laden. Wês foarsichtich mei it útskeakeljen fan ûnbekende tsjinsten."

#~ msgid "Use"
#~ msgstr "Brûke"

#~ msgid "Start"
#~ msgstr "Begjinne"

#~ msgid "Stop"
#~ msgstr "Ophâlde"

#~ msgid "Unable to contact KDED."
#~ msgstr "Der koe gjin kontakt makke wurde mei KDED."

#~ msgid "Unable to start service <em>%1</em>.<br /><br /><i>Error: %2</i>"
#~ msgstr ""
#~ "It úteinsetten fan tsjinner <em>%1</em> slagge net.<br /><br /><i>Flater: "
#~ "%2</i>"

#~ msgid "Unable to stop service <em>%1</em>.<br /><br /><i>Error: %2</i>"
#~ msgstr ""
#~ "It ophâlden fan tsjinner <em>%1</em> slagge net.<br /><br /><i>Flater: "
#~ "%2</i>"
