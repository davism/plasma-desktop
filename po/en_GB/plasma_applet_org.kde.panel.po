# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Steve Allewell <steve.allewell@gmail.com>, 2018, 2019, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-14 00:48+0000\n"
"PO-Revision-Date: 2022-07-22 16:41+0100\n"
"Last-Translator: Steve Allewell <steve.allewell@gmail.com>\n"
"Language-Team: British English <kde-l10n-en_gb@kde.org>\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.04.3\n"

#: contents/ui/ConfigOverlay.qml:260 contents/ui/ConfigOverlay.qml:295
#, kde-format
msgid "Remove"
msgstr "Remove"

#: contents/ui/ConfigOverlay.qml:270
#, kde-format
msgid "Configure…"
msgstr "Configure…"

#: contents/ui/ConfigOverlay.qml:280
#, kde-format
msgid "Show Alternatives…"
msgstr "Show Alternatives…"

#: contents/ui/ConfigOverlay.qml:305
#, kde-format
msgid "Spacer width"
msgstr "Spacer width"
