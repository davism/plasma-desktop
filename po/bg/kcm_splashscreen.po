# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Mincho Kondarev <mkondarev@yahoo.de>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-30 00:49+0000\n"
"PO-Revision-Date: 2022-12-01 23:51+0100\n"
"Last-Translator: Mincho Kondarev <mkondarev@yahoo.de>\n"
"Language-Team: Bulgarian <kde-i18n-doc@kde.org>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.08.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: kcm.cpp:160
#, kde-format
msgid "None"
msgstr "Без"

#: kcm.cpp:162
#, kde-format
msgid "No splash screen will be shown"
msgstr "Няма да се покаже начален екран"

#: kcm.cpp:180
#, kde-format
msgid "You cannot delete the currently selected splash screen"
msgstr "Не можете да изтриете на текущо избрания начален екран"

#: package/contents/ui/main.qml:16
#, kde-format
msgid "This module lets you choose the splash screen theme."
msgstr "Този модул ви позволява да изберете темата за началния екран."

#: package/contents/ui/main.qml:30
#, kde-format
msgid "&Get New…"
msgstr "Изте&гляне на нови…"

#: package/contents/ui/main.qml:51
#, kde-format
msgid "Failed to show the splash screen preview."
msgstr "Неуспешно показване на предварителен преглед за начален екран."

#: package/contents/ui/main.qml:83
#, kde-format
msgid "Preview Splash Screen"
msgstr "Преглед на началния екран"

#: package/contents/ui/main.qml:88
#, kde-format
msgid "Uninstall"
msgstr "Деинсталиране"

#. i18n: ectx: label, entry (engine), group (KSplash)
#: splashscreensettings.kcfg:12
#, kde-format
msgid "For future use"
msgstr "За бъдеща употреба"

#. i18n: ectx: label, entry (theme), group (KSplash)
#: splashscreensettings.kcfg:16
#, kde-format
msgid "Name of the current splash theme"
msgstr "Име на текущата тема за начален екран"
